import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { Config } from '../config';
import { Router, ActivatedRoute } from '@angular/router';


@Injectable({providedIn: 'root'})
export class UserService {

    private authEnd = Config.Auth_Endpoint;
    private demoEnd = Config.Demo_Endpoint;

    private currentUserSubject: BehaviorSubject<any>;
    public currentUser: Observable<any>;

    constructor(
        private httpClient: HttpClient,
        private route: ActivatedRoute,
        private router: Router
    ) {
        this.currentUserSubject = new BehaviorSubject<any>(localStorage.getItem('currentUser'));
    }
    
    public get currentUserValue() {
        return this.currentUserSubject.value;
    }

    loginPost(data: any){
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type'  : 'application/x-www-form-urlencoded'
            })
        }
        const urlSearchParams = new HttpParams()
            .set('identity', data.input1)
            .set('password', data.input2)
        const body = urlSearchParams.toString();
        return this.httpClient.post(this.authEnd, body, httpOptions)
                              .toPromise();
    }

    userData(data:any) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type'  : 'application/x-www-form-urlencoded',
                'Authorization' : 'Bearer ' + sessionStorage.getItem('token'),
            })
        }
        return this.httpClient.get(this.demoEnd + data, httpOptions)
                              .toPromise();
    }

    create(data:any){

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type'  : 'application/x-www-form-urlencoded',
                'Authorization' : 'Bearer ' + sessionStorage.getItem('token'),
            })
        }

        const urlSearchParams = new HttpParams()
            .set('nip', data.input_nip)
            .set('full_name', data.input_full_name)
            .set('nick_name', data.input_nick_name)
            .set('birth_date', data.input_birth_date)
            .set('address', data.input_address)
            .set('phone', data.input_phone)
            .set('mobile', data.input_mobile)
            .set('email', data.input_email)
        const body = urlSearchParams.toString();

        return this.httpClient.post(this.demoEnd, body, httpOptions)
                              .toPromise(); 
    }

    employeeList(){
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type'  : 'application/x-www-form-urlencoded',
                'Authorization' : 'Bearer ' + sessionStorage.getItem('token'),
            })
        }
        return this.httpClient.get(this.demoEnd + 'page-search', httpOptions)
                              .toPromise();
    }

    logout(){
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
        window.location.reload();
    }
    
}