import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeComponent } from './employee/employee.component';
import { HomeComponent } from './home/home.component';
import { PageComponent } from './page.component';
import { ProfileComponent } from './profile/profile.component';


const pageRoutes: Routes = [
  { 
    path: '', 
    component: PageComponent,
    children: [
    { path: '', redirectTo: '/dashboard', pathMatch: 'full'},
    { path: 'dashboard', component: HomeComponent},
    { path: 'employee', component: EmployeeComponent},
    { path: 'profile', component: ProfileComponent},
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(pageRoutes)],
  exports: [RouterModule]
})
export class PageRoutingModule { }
