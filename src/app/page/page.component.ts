import { Component, OnInit } from '@angular/core';
import { User } from '../services/user';
import { UserService } from '../services/user.services';

@Component({
    selector: 'page-app',
    templateUrl: 'page.component.html',
    styleUrls: ['./page.component.css'],
    providers: [ UserService ]
})

export class PageComponent implements OnInit {

    userCurrent:string = localStorage.getItem('currentUser');
    constructor(
        private userService: UserService
    ) { }

    ngOnInit() {
        window.scrollTo(0, 0);
     }

    logout(){
        this.userService.logout();
    }
}