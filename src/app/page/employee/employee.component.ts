import { Component, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { HttpClient } from '@angular/common/http';
import { UserService } from 'src/app/services/user.services';

import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { DialogComponent } from 'src/app/component/dialog/dialog.component';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent {

  dialog:boolean = false;
  loading: boolean = false;
  employees: any[];
  dataSource = new MatTableDataSource<any>();

  //input
  fieldNip:string     = '';
  fieldName:string    = '';
  fieldNick:string    = '';
  fielddate:string    = '';
  fieldAddress:string = '';
  fieldPhone:string   = '';
  fieldMobile:string  = '';
  fieldEmail:string   = '';

  responsePost:string;

  // @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private http: HttpClient,
    private userService: UserService,
  ) {
  }

  ngOnInit() {
    // this.getData();
  }

  getData(){
    return this.userService.employeeList()
                           .then(
                             (response:any) => {
                               console.log(response);
                               this.loading = false;
                              this.employees = response.users;
                              this.dataSource = new MatTableDataSource<any>(this.employees);
                              // this.dataSource.paginator = this.paginator;
                             }
                           )
  }

  async postData(){
    this.loading = true;
    return this.userService.create({ input_nip: this.fieldNip, input_full_name: this.fieldName, input_nick_name: this.fieldNick, input_birth_date: this.fielddate, input_address: this.fieldAddress, input_phone: this.fieldPhone, input_mobile: this.fieldMobile, input_email: this.fieldEmail
                                  })
                           .then(
                             (response:any) => {
                               if(200 === response.status){
                                this.loading = false;
                                this.responsePost = response.message;
                               }
                             }
                             ,
                            (error) => {
                              this.loading = false;
                              this.responsePost = error.error.message;
                              console.log(error);
                            }
                           )
  }

}
