import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.services';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [ UserService ]
})
export class ProfileComponent implements OnInit {

  userData:any = [];
  loading: boolean = true;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
    this.loadUserData();
  }

  async loadUserData(){
    return this.userService.userData(localStorage.getItem('currentUserId'))
                    .then(
                      (result:any) => {
                        console.log(result);
                        this.loading = false;
                        this.userData = result;
                      }
                    )
  }

}
