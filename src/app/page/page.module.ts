import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';

import { EmployeeComponent } from './employee/employee.component';
import { HomeComponent } from './home/home.component';
import { PageRoutingModule } from './page-routing.module';
import { PageComponent } from './page.component';
import { ProfileComponent } from './profile/profile.component';
import { DialogComponent } from '../component/dialog/dialog.component';

@NgModule({
    declarations: [
        PageComponent,
        HomeComponent,
        EmployeeComponent,
        ProfileComponent,
        DialogComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PageRoutingModule,
        MatProgressSpinnerModule,
        MatFormFieldModule,
        MatCardModule,
        MatGridListModule,
        MatPaginatorModule,
        MatSortModule,
        MatTableModule,
        MatDialogModule
    ],
    exports: [
        MatProgressSpinnerModule,
        MatFormFieldModule,
        MatCardModule,
        MatGridListModule,
        MatPaginatorModule,
        MatSortModule,
        MatTableModule,
        MatDialogModule
    ],
    providers: [],
})
export class PageModule { }
