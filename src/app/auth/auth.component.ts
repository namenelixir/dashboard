import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { UserService } from '../services/user.services';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
  providers: [UserService]
})
export class AuthComponent implements OnInit {

  data: any = [];

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string;

  username: string = '';
  password: string = '';

  constructor(
    private formBuilder: FormBuilder,
    private service: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private authService : UserService
  ) {
    if (this.authService.currentUserValue) { 
      this.router.navigate(['/']);
  }
  }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  

  async loginAuth(){
    this.loading = true;
    return this.service.loginPost({input1: this.username, input2: this.password})
                       .then(
                          (result: any) => {
                            if(200 === result.status) {
                              this.loading = false;
                              sessionStorage.setItem('token', result.data.token.access_token);
                              sessionStorage.setItem('token_expr', result.data.token.expires_in);
                              localStorage.setItem('currentUser', result.data.user.username);
                              localStorage.setItem('currentUserId', result.data.user.id);
                              this.router.navigate(['/']);
                              window.location.reload();
                            }
                          },
                          (error) => {
                            this.loading = false;
                            this.error = error.error.message;
                            console.log(error);
                          }
                        );
  }

}
